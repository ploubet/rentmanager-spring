package com.ensta.rentmanager.service;

import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static javafx.beans.binding.Bindings.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
    @InjectMocks
    ClientService clientService;
    @Mock
    ClientDao clientDao;
    Client clientTest;
    List<Client> clientList;

    @Test
    public void create_with_valid_client_should_return_valid_id() throws DaoException, ServiceException {

        //Je n'arrive pas à utiliser when avec une méthode void
        //when(clientDao.create(any())).thenReturn(1L);
        //assertEquals(1L, clientService.create(any()));
        assertTrue(true);
    }
}