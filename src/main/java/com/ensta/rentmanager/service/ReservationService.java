package com.ensta.rentmanager.service;

import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {

    private Reservation reservation;
    private ReservationDao reservationDao;

    @Autowired
    private ReservationService (ReservationDao reservationDao){
        this.reservationDao = reservationDao;
    }

    public long create(Reservation reservation) throws ServiceException {
        //Permet de contrôler que le champ de type “email” contient une extension “.com” ou “.fr”
        if (reservation.getDebut().isBefore(reservation.getFin()) ) {
        try {
            return reservationDao.create(reservation);
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la cr�ation de la reservation.");
        }
        }
        else {
            System.out.println("La date de début doit être avant la date de fin.");
            return 0;
        }
    }

    public List<Reservation> findAll() throws ServiceException {
        List<Reservation> listeReservations = new ArrayList<Reservation>();
        try {
            listeReservations = reservationDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la consultation des reservations.");
        }
        return listeReservations;
    }

    public Reservation findById(long id) throws ServiceException {
        Optional<Reservation> optionalReservation = Optional.empty();
        try {
            optionalReservation = reservationDao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la consultation de la reservation "+id+" .");
        }
        return optionalReservation.get();
    }

    public Long nbReservation() throws ServiceException {
        try {
            return reservationDao.count();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors du dénombrement.");
        }
    }

    public long delete(Reservation reservation) throws ServiceException {
        try {
            reservationDao.delete(reservation);
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la suppression de la reservation.");
        }
        return 0;
    }
}
