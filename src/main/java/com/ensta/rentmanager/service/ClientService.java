package com.ensta.rentmanager.service;

import java.sql.Connection;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ClientService {
	private ClientDao clientDao;
	private Client client;

	@Autowired
	public ClientService(ClientDao clientDao){
		this.clientDao = clientDao;
	}

	public long create(Client client) throws ServiceException {
		//Contrôle si le client est majeur.
		if ( (Period.between(client.getNaissance(), LocalDate.now()).getYears()) <= 18 )
			throw new ServiceException("Le client doit être majeur !");
		try {
			clientDao.create(client);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la cr�ation du client.");	
		}
		return 0;
	}

	public Client findById(long id) throws ServiceException {
		Optional<Client> optionalClient = Optional.empty();
		try {
			optionalClient = clientDao.findById(id);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la consultation du client "+id+" .");	
		}
		return optionalClient.get();
	}

	public List<Client> findAll() throws ServiceException {
		List<Client> listeClients = new ArrayList<Client>();
		try {
			listeClients = clientDao.findAll();
			//System.out.println(listeClients);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la consultation des clients.");	
		}
		return listeClients;
	}
		
	public long delete(Client client) throws ServiceException {
		try {
			clientDao.delete(client);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la suppression du client.");	
		}
		return 0;
	}

    public Long nbClient() throws ServiceException {
        try {
            return clientDao.count();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors du dénombrement.");
        }
    }
}
