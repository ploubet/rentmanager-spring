package com.ensta.rentmanager.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.VehicleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class VehicleService {

    private VehicleDao vehiculeDao;
    private Vehicule vehicule;
    @Autowired
    public VehicleService(VehicleDao vehiculeDao){
        this.vehiculeDao = vehiculeDao;
    }

    public long create(Vehicule vehicule) throws ServiceException {
        if (vehicule.getConstructeur()== "" || vehicule.getModele()== "") {
            throw new ServiceException("Le constructeur et le modele doivent �tre renseign�s.");
        }
        try {
            vehiculeDao.create(vehicule);
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la cr�ation du vehicule.");
        }
        return 0;
    }//fin de la m�thode create

	public Vehicule findById(long id) throws ServiceException {
		// TODO: Recuperer un vehicule par son id
        Optional<Vehicule> optionalVehicule = Optional.empty();
        try {
            optionalVehicule = vehiculeDao.findById(id);
            optionalVehicule.get().toString();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la consultation du vehicule "+id+" .");
        }
        return optionalVehicule.get();
    }

	public List<Vehicule> findAll() throws ServiceException {
        List<Vehicule> vehicules = new ArrayList<Vehicule>();
        try {
            vehicules = vehiculeDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la consultation des clients.");
        }
        return vehicules;
    }

    public Long nbVehicule() throws ServiceException {
        try {
            return vehiculeDao.count();
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors du dénombrement.");
        }
    }

    public long delete(Vehicule vehicule) throws ServiceException {
        try {
            vehiculeDao.delete(vehicule);
        } catch (DaoException e) {
            throw new ServiceException("Une erreur a eu lieu lors de la suppression du véhicule.");
        }
        return 0;
    }

}
