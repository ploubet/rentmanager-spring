package com.ensta.rentmanager.ui.controller;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("rents")
public class ReservationController {
    private ReservationService reservationService;
    private VehicleService vehiculeService;
    private ClientService clientService;

    private ReservationController (ReservationService reservationService, VehicleService vehiculeService, ClientService clientService){
        this.reservationService = reservationService;
        this.vehiculeService = vehiculeService;
        this.clientService = clientService;
    }

    @GetMapping("list")
    public String get(Model model){
        java.util.List<Reservation> mesReservations = new ArrayList<Reservation>();
        try {
            mesReservations = this.reservationService.findAll();
            model.addAttribute("reservations", mesReservations);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return "rents/list";
    }

    @GetMapping("delete/{id}")
    public RedirectView delete(@PathVariable("id") int id, Model model){
        try {
            reservationService.delete(this.reservationService.findById(id));
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return new RedirectView ("/rentmanager/rents/list");
    }

    @GetMapping("create")
    public String create(Model model){
        try {
            //Récupération de la liste des voitures
            java.util.List<Vehicule> vehicules = new ArrayList<Vehicule>();
            vehicules = this.vehiculeService.findAll();
            model.addAttribute("Vehicules", vehicules);
            //Récupération de la liste des clients
            java.util.List<Client> mesClient = new ArrayList<Client>();
            mesClient = this.clientService.findAll();
            model.addAttribute("Clients", mesClient);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return "rents/create";
    }

    @PostMapping("create")
    public RedirectView create(Model model, @ModelAttribute() final Reservation reservation){
            try {
                this.reservationService.create(reservation);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        return new RedirectView ("/rentmanager/rents/list");
    }
}
