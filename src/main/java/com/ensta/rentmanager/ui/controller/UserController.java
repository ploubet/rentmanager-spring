package com.ensta.rentmanager.ui.controller;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("users")
public class UserController {

    private ClientService clientService;

    public UserController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("list")
    public String get(Model model){
        try {
            java.util.List<Client> mesClient = new ArrayList<Client>();
            mesClient = this.clientService.findAll();
            model.addAttribute("Clients", mesClient);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return "users/list";
    }

    @GetMapping("create")
    public String create(Model model){ return "users/create"; }

    @PostMapping("create")
    public RedirectView create(Model model, @ModelAttribute() final Client client){
        //Permet de contrôler que le champ de type “email” contient une extension “.com” ou “.fr”
        if (client.getEmail().contains(".fr") || client.getEmail().contains(".com")) {
            try {
                this.clientService.create(client);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Erreur email");
            //Retourne une erreur dans la page list.jsp -- BUG
            model.addAttribute("erreur", "L'adresse mail est invalide.");}
        return new RedirectView ("/rentmanager/users/list");
    }

    @GetMapping("details/{id}")
    public String details(@PathVariable("id") int id, Model model){
        Client client = new Client();
        try {
             client = this.clientService.findById(id);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        model.addAttribute("client", client);
        return "users/details";
    }

    @GetMapping("delete/{id}")
    public RedirectView delete(@PathVariable("id") int id, Model model){
        try {
            Client client = new Client();
            client = this.clientService.findById(id);
            clientService.delete(client);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return new RedirectView ("/rentmanager/users/list");
    }

    }