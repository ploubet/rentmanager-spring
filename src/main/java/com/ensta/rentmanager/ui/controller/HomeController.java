package com.ensta.rentmanager.ui.controller;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeController {
    public HomeController (ClientService clientService, VehicleService vehiculeService, ReservationService reservationService){
        this.clientService = clientService;
        this.vehiculeService = vehiculeService;
        this.reservationService = reservationService;
    }
    private ReservationService reservationService;
    private ClientService clientService;
    private VehicleService vehiculeService;

    @GetMapping
    public String get(Model model){
        Long nbClients = 0L; Long nbVehicules = 0L; Long nbReservations = 0L;
        try {
            nbClients = this.clientService.nbClient();
            nbVehicules = this.vehiculeService.nbVehicule();
            nbReservations = this.reservationService.nbReservation();
        } catch ( ServiceException e) {
            e.printStackTrace();
        }
        model.addAttribute("nbClients", nbClients);
        model.addAttribute("nbVehicules", nbVehicules);
        model.addAttribute("nbReservations", nbReservations);
        return "home";
    }
}
