package com.ensta.rentmanager.ui.controller;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("vehicles")
public class VehiculeController {
    private VehicleService vehiculeService;

    public VehiculeController(VehicleService vehiculeService) {
        this.vehiculeService = vehiculeService;
    }

    @GetMapping("list")
    public String get(Model model){
        try {
            java.util.List<Vehicule> vehicules = new ArrayList<Vehicule>();
            vehicules = this.vehiculeService.findAll();
            model.addAttribute("Vehicules", vehicules);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return "vehicles/list";
    }

    @GetMapping("create")
    public String create(Model model){
        return "vehicles/create";
    }

    @PostMapping("create")
    public RedirectView create(Model model, @ModelAttribute() Vehicule vehicule){
        try {
            this.vehiculeService.create(vehicule);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return new RedirectView ("/rentmanager/vehicles/list");
    }

    @GetMapping("delete/{id}")
    public RedirectView delete(@PathVariable("id") int id, Model model){
        try {
            Vehicule vehicule = new Vehicule();
            vehicule = this.vehiculeService.findById(id);
            vehiculeService.delete(vehicule);
            model.addAttribute("vehicule", vehicule);
        } catch(ServiceException e){
            e.printStackTrace();
        }
        return new RedirectView ("/rentmanager/vehicles/list");
    }
}
