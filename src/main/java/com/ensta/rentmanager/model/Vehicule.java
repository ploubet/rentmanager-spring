package com.ensta.rentmanager.model;

import javax.persistence.*;

@Entity
@Table(name="Vehicule")
public class Vehicule {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String constructeur;
	private String modele;
	private int nb_places;
	
	//Constructeur	
	public Vehicule() {}

	public Vehicule(Long id, String constructeur, String modele, int nb_places) {
		this.id = id;
		this.constructeur = constructeur;
		this.modele = modele;
		this.nb_places = nb_places;
	}
	public Vehicule(String constructeur, String modele, int nb_places) {
	this.constructeur = constructeur;
	this.modele = modele;
	this.nb_places = nb_places;
	}
	
	//Getters
	public String getConstructeur() { return constructeur; }
	public String getModele() { return modele; }
	public int getNb_places() { return nb_places; }
	public Long getId() {return id;}
	
	
	//Setters
	public void setConstructeur(String constructeur) { this.constructeur = constructeur; }
	public void setModele(String modele) { this.modele = modele; }
	public void setNb_places(int nb_places) { this.nb_places = nb_places; }
	public void setId(Long id) { this.id = id; }
	
	public String toString() {
		return "Vehicule{" +	"constructeur=" + constructeur +	", modele=" + modele
				+	", nb_places=" + nb_places + '\'' +	'}';
		}
}
