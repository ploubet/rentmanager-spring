package com.ensta.rentmanager.model;

//Pour utiliser le type LocalDate
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="Client")
public class Client {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String prenom;
	private String email;
	//Permet de saisir une chaîne de caractère dans la jsp au format 10/01/2109
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate naissance;
	
	//Constructeurs
	public Client() {
	}
	
	public Client(Long id, String nom, String prenom, String email, LocalDate naissance) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.naissance = naissance;
		}

	public Client( String nom, String prenom, String email) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}
	
	public Client(String nom, String prenom, String email, LocalDate naissance) {
	this.nom = nom;
	this.prenom = prenom;
	this.email = email;
	this.naissance = naissance;
	}
	
	//Getters
	public String getNom() { return nom; }
	public String getPrenom() { return prenom; }
	public String getEmail() { return email; }
	public LocalDate getNaissance() { return naissance; }
	public Long getId() { return id; }
	
	//Setters
	public void setId(Long id) { this.id = id; }
	public void setNom(String nom) { this.nom = nom; }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setEmail(String email) { this.email = email; }
	public void setNaissance(LocalDate naissance) { this.naissance = naissance; }

	public String toString() {
		return "Client{" +	"nom=" + nom +	", prenom=" + prenom 
				+	", email=" + email +	", naissance=" + naissance +	"}";
		}
	}
