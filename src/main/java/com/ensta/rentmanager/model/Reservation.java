package com.ensta.rentmanager.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="Reservation")
public class Reservation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long client_id;
	private Long vehicle_id;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate debut;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fin;
	
	//Constructeur
	public Reservation(){}
	public Reservation(Long client_id, Long vehicle_id, LocalDate debut, LocalDate fin) {
	this.client_id = client_id;
	this.vehicle_id = vehicle_id;
	this.debut = debut;
	this.fin = fin;
	}
	
	//Getters
	public Long getId() { return id; }
	public Long getClient_id() { return client_id; }
	public Long getVehicle_id() { return vehicle_id; }
	public LocalDate getDebut() { return debut; }
	public LocalDate getFin() { return fin; }
	
	//Setters
	public void setClient_id(Long client_id) { this.client_id = client_id; }
	public void setVehicle_id(Long vehicle_id) { this.vehicle_id = vehicle_id; }
	public void setDebut(LocalDate debut) { this.debut = debut; }
	public void setFin(LocalDate fin) { this.fin = fin; }
	
	public String toString() {
		return "Reservation{" +	"client_id=" + client_id +	", vehicle_id=" + vehicle_id 
				+	", debut=" + debut +	", fin =" + fin + '\'' +	'}';
		}
	}
