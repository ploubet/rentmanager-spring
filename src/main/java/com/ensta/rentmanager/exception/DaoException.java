package com.ensta.rentmanager.exception;

public class DaoException extends Exception {

	public DaoException() {
		System.out.println("Exception dans le DAO");
	}
	
	public DaoException(String s) {
		System.out.println(s);
		System.out.println("Exception dans le DAO");
	}
}
