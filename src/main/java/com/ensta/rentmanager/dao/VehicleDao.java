package com.ensta.rentmanager.dao;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.configuration.AppConfiguration;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;

@Repository
public class VehicleDao {
    //injection connection
    private Connection maConnection;
    //JAVA Persistence API
    private final Session session;

    @Autowired
    public VehicleDao(Connection maConnection, Session session){
        this.maConnection = maConnection;
        this.session = session;
    }

	public long create(Vehicule vehicle) throws DaoException {
       try {
            return (long) session.save(vehicle);
        } catch (PersistenceException e) {
            throw new DaoException();
        }
    }

	public long delete(Vehicule vehicle) throws DaoException {
        try {
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        } catch (PersistenceException e) {
            throw new DaoException();
        }
        return 0;
    }

	public Optional<Vehicule> findById(long id) throws DaoException {
        try {
            Vehicule vehicle = session.find(Vehicule.class, id);
            return Optional.ofNullable(vehicle);
        } catch (PersistenceException e) {
            throw new DaoException();
        }
    }

	public List<Vehicule> findAll() throws DaoException {
        try {
            Query query = session.createQuery("SELECT v FROM Vehicule v");
            return (List<Vehicule>) query.getResultList();
        } catch (PersistenceException e) {
            throw new DaoException();
        }
    }

    public Long count() throws DaoException {
        try {
            Query query = session.createQuery("SELECT COUNT(*) FROM Vehicule v");
            return (Long) query.uniqueResult();
        } catch (PersistenceException e) {
            throw new DaoException();
        }
    }
}