package com.ensta.rentmanager.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;

@Repository
public class ReservationDao {
	//injection connection
	private Connection maConnection;
	//JAVA Persistence API
	private final Session session;

	@Autowired
	public ReservationDao(Connection maConnection, Session session){
		this.maConnection = maConnection;
		this.session = session;
	}

	public long create(Reservation reservation) throws DaoException {
		try {
			session.save(reservation);
			return 1;
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public long delete(Reservation reservation) throws DaoException {
		try {
			session.beginTransaction();
			session.delete(reservation);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
		return 0;
	}
	
	public List<Reservation> findResaByClientId(long clientId) throws DaoException {
		try {
			Query query = session.createQuery("SELECT r FROM Reservation r WHERE r.client_id=:client");
			query.setParameter("client", clientId);
			return (List<Reservation>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public List<Reservation> findResaByVehicleId(long vehicleId) throws DaoException {
		try {
			Query query = session.createQuery("SELECT r FROM Reservation r WHERE r.vehicle_id=:vehicule");
			query.setParameter("vehicule", vehicleId);
			return (List<Reservation>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}

	public List<Reservation> findAll() throws DaoException {
		try{
			Query query = session.createQuery("SELECT r FROM Reservation r");
			return (List<Reservation>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}

	public Optional<Reservation> findById(long id) throws DaoException {
		try {
			Reservation reservation = session.find(Reservation.class, id);
			return Optional.ofNullable(reservation);
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}

	public Long count() throws DaoException {
		try {
			Query query = session.createQuery("SELECT COUNT(*) FROM Reservation");
			return (Long) query.uniqueResult();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
}
