package com.ensta.rentmanager.dao;

import java.util.List;

import java.sql.Connection;
import java.util.Optional;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Client;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.PersistenceException;

@Repository
public class ClientDao {
	//injection connection
	private Connection maConnection;
	//JAVA Persistence API
	private final Session session;

	@Autowired
    public ClientDao(Connection maConnection, Session session){
    	this.maConnection = maConnection;
		this.session = session;
	}

	//CREATION d'un CLIENT
	public void create(Client client) throws DaoException {
		try {
			session.save(client);
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public long delete(Client client) throws DaoException {
		try {
			session.beginTransaction();
			session.delete(client);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
		return 0;
	}

	public Optional<Client> findById(long id) throws DaoException {
		try {
			Client client = session.find(Client.class, id);
			return Optional.ofNullable(client);
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}

	public List<Client> findAll() throws DaoException {
		try{
			Query query = session.createQuery("SELECT c FROM Client c");
			return (List<Client>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}

    public Long count() throws DaoException {
		try{
			Query query = session.createQuery("SELECT COUNT(*) FROM Client");
			return (Long) query.uniqueResult();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
    }
}