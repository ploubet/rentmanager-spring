package com.ensta.rentmanager.configuration;
import com.ensta.rentmanager.persistence.ConnectionManager;

import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.Session;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
@ComponentScan({ "com.ensta.rentmanager.model", "com.ensta.rentmanager.service", "com.ensta.rentmanager.dao", "com.ensta.rentmanager.mapper" }) // packages dans lesquels chercher les beans
@EnableWebMvc
public class AppConfiguration implements WebMvcConfigurer {
    @Bean
    public Connection jdbcConnection() throws SQLException {
        return new ConnectionManager().getConnection();
    }
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.ensta.rentmanager.model");
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:~/h2.database/RentManagerDatabase");
        dataSource.setUser("");
        dataSource.setPassword("");
        return dataSource;
    }

    @Bean
    public Session getSession(LocalSessionFactoryBean localSessionFactoryBean) {
        return localSessionFactoryBean.getObject().openSession();
    }
}
