# Projet ASI 311
"Application de gestion de location de véhicules" par Pierrick et Mehdi

## Liste des fonctionnalités minimales implémentées:

### Véhicules (3/3)
- Afficher la liste des véhicules présents dans la base de données
- Insérer un véhicule dans la base de données
- Supprimer un véhicule

### Clients (3/3)
- Afficher la liste des clients présents dans la base de données
- Insérer un client dans la base de données avec la date de naissance saisie sous forme de String dans la JSP (format 10/01/2019) et convertie en LocalDate dans le modèle.
- Supprimer un client

### Page d’accueil (2/2)
- Afficher le nombre de véhicules présents dans la base de données
- Afficher le nombre d’utilisateurs présents dans la base de données

## Contraintes de validation (3/3)
- Technique : adresse mail avec "@" et domaine en ".fr" ou ".com" dans le contrôleur et la JSP
- Fonctionnelle : un client doit être majeur (dans le service)
- Fonctionnelle : la date de début de réservation doit être avant la date de fin (dans le service)

## Liste des fonctionnalités avancées implémentées:

### Réservations (3/3)
- Afficher la liste des réservations présentes dans la base de données
- Insérer une réservation dans la base de données
- Supprimer une réservation

### Page d’accueil (1/1)
- Afficher le nombre de réservations présentes dans la base de données

### Créer la page de profil d’un client (0/2)
- Afficher le nombre de réservations d’un client
- Afficher les réservations d’un client

### Modifier la base de données pour lier une voiture à un client (son propriétaire) (0/1)

### Modifier le formulaire d’insertion de véhicule pour permettre à l’utilisateur de sélectionner le propriétaire du véhicule (0/1)

### Page de profil d’un client  (0/1)
- Afficher le nombre de véhicules appartenant à ce client
- Afficher les véhicules appartenant à ce client

## Liste des bugs rencontrés :
- Gestion des erreurs - nous n'avons pas réussi à exploiter et afficher les exceptions via getStackTrace
- Afficher le nom des véhicules et des clients dans la liste des reservations au lieu des id